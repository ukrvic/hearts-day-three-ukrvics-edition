# hearts_day_second
...


# Rules for the Card Game Hearts

## Objective

- Hearts is a trick-taking game played with 4 players using a standard 52-card deck. 

- The objective is to avoid scoring points by avoiding capturing hearts and the queen of spades.

## Gameplay

- Each round begins with one player leading a card. Going clockwise, each player must follow suit if able. If they cannot follow suit, they may play any card.

- The player who played the highest card in the suit led wins the trick and leads the next trick.

- Hearts cannot be led until they have been "broken" - played on another suit. 

- The queen of spades is worth 13 penalty points and hearts are worth 1 penalty point each. There are 26 total points per round.

- After each round, players count their penalty points and the player with the lowest score wins. If one player reaches 100 penalty points, the game ends. 

## Variations

- There are some variations on the rules, including passing cards between players before each round. But those are some of the standard rules and gameplay basics for Hearts.

# Created by ukrvic for Cyberbionic marahpon Heart`s card game. 

