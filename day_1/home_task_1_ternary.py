from numbers import Number
from typing import Dict

dict1 = {}
st1 = "asdfadfaadsf"


def get_char_frequency(st1: str) -> Dict[str, float]:
    """
    Calculates the frequency of each character in the input string.

    Args:
        reciving (str): The string to analyze

    Returns:
        dict: A dictionary with keys as characters and values as the frequency percentage

    Example:
        get_char_frequency("Hello World")
        Would return:
        {'H': '0.09', 'e': '0.18', 'l': '0.36', 'o': '0.27', ' ': '0.09', 'W': '0.09', 'r': '0.09', 'd': '0.09'}

    This calculates the percentage frequency of each character in the string
    rounded to 2 decimal places and returns the frequencies in a dictionary.
    """
    for ch in st1:
        dict1[ch] = format((st1.count(ch) / len(st1)), ".2f")
    return dict1


# Start exception area for Black linter
# fmt: off
def get_ternary(x, y : Number) -> str:
    """
        Returns the result of an operation on x and y as a string.

        Args:
            x: First number
            y: Second number

        Returns:
            - If x < y, returns string sum of x + y
            - If x > y, returns difference x - y
            - If x and y are both 0, returns string "game over"
            - Otherwise, returns string "0"

        Examples:
            >>> get_ternary(1, 2)
            "3"

            >>> get_ternary(3, 1)
            "2"

            >>> get_ternary(0, 0)
            "game over"
        """
    return str(
        x + y if (x < y) else
        x - y if (x > y) else
        "game over" if (x == 0 and y == 0) else
        0
    )
# fmt: on
# End exception area for Black linter
if __name__ == "__main__":
    print(get_char_frequency("adfdfafhjghgyygndfzcvcvzzv"))
    cases = (
        (1, 2, "3"),
        (1, 1, "0"),
        (2, 1, "1"),
        (0, 0, "game over"),
    )
    for x, y, result in cases:
        func_res = get_ternary(x, y)
        assert func_res == result, print(f"Error")

    print("Done")
